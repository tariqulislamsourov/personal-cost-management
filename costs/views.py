from django.shortcuts import render, redirect
from django.db.models import Count, Sum
from costs.forms import CostRelatedForm, CostForm
from django.core.paginator import Paginator
from costs.models import Costs, CostRelated
from datetime import datetime, date, timedelta
from savings.models import Wallet
# Create your views here.

def eachPageObject(request, obj_list, obj_per_page, per_page_link):

    paginator = Paginator(obj_list, obj_per_page)
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)

    context = {
        'pagination_data_list': page_obj, 
        'per_page_objects': obj_per_page, 
        'per_page_link': per_page_link
    }

    return context

def create_first_last_date(current_date):
    # current_date = date.today()
    year = current_date.year
    month = current_date.month

    first_date_of_this_month = datetime(year, month, 1)
    prev_month_last_day = first_date_of_this_month - timedelta(days=1)
    prev_month_first_day = prev_month_last_day.replace(day=1)
    if current_date.month == 2:
        try:                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          
            last_date_of_this_month = datetime(year, month, 28)
        except:
            last_date_of_this_month = datetime(year, month, 29)
    else:
        try:
            last_date_of_this_month = datetime(year, month, 31)
        except:
            last_date_of_this_month = datetime(year, month, 30)
    print(current_date.month)

    next_month_first_day = last_date_of_this_month + timedelta(days=1)
    add_day_to_next_month = next_month_first_day.replace(day=28) + timedelta(days=4)
    next_month_last_day = add_day_to_next_month - timedelta(days=add_day_to_next_month.day)

    print(first_date_of_this_month.date())
    print(last_date_of_this_month)
    print(prev_month_first_day)
    print(prev_month_last_day)
    print(next_month_first_day)
    print(next_month_last_day)


    return first_date_of_this_month.date(), last_date_of_this_month.date(), prev_month_first_day.date(), prev_month_last_day.date(), next_month_first_day.date(), next_month_last_day.date()

def UserCostList(request):
    template_name = 'costs/cost_list.html'
    cost_related_form = CostRelatedForm
    cost_form = CostForm
    if request.GET.get('date_to') == None and request.GET.get('date_from') == None:
        current_date = date.today()

    first_date_of_this_month, last_date_of_this_month, prev_month_first_day, prev_month_last_day, next_month_first_day, next_month_last_day = create_first_last_date(current_date)
    all_my_wallet = Wallet.objects.filter(wallet_of = request.user)
    all_cost_list = Costs.objects.filter(cost_related_id__create_by_id = request.user.id, cost_date__range = [first_date_of_this_month, last_date_of_this_month]).order_by('cost_date')
    total_cost_of_running_month = all_cost_list.aggregate(the_count=Sum('amount'))
    print(total_cost_of_running_month['the_count'])
    all_cost_related_by_this_user = CostRelated.objects.filter(create_by_id= request.user.id)

    if request.method == "POST":
        cost_related_form = CostRelatedForm(request.POST)
        cost_form = CostForm(request.POST)
        wallet = Wallet.objects.get(id = int(request.POST.get('wallet')))
        cost_amount = int(request.POST.get('amount'))
        print("-------------")
        print(request.POST.get('select_short_info'))

        if cost_related_form.is_valid():
            cost_related_form = cost_related_form.save(commit=False)
            cost_related_form.create_by_id = int(request.user.id)
            print(cost_related_form.create_by_id)
            cost_related_form.save()

            last_entry_by_this_user = CostRelated.objects.filter(create_by = request.user, short_info = cost_related_form.short_info).latest('created_at')
            print(cost_form)
            if cost_form.is_valid():
                cost_form = cost_form.save(commit=False)
                cost_form.cost_related_id_id = int(last_entry_by_this_user)
                print(cost_form.cost_related_id_id)
                print(cost_form)
                cost_form.save()
                wallet.wallet_status = wallet.wallet_status - cost_amount
                wallet.save()

        elif cost_related_form.is_valid()==False and request.POST.get('select_short_info') != '':
            prev_cost_related_info = request.POST.get('select_short_info')
            print(cost_related_form.is_valid())
            if cost_form.is_valid():
                cost_form = cost_form.save(commit=False)
                cost_form.cost_related_id_id = int(prev_cost_related_info)
                print(cost_form.cost_related_id_id)
                print(cost_form)
                cost_form.save()
                wallet.wallet_status = wallet.wallet_status - cost_amount
                wallet.save()

        return redirect('costs:my_costs')

    obj_per_page = 100 # number of items to be shown per page
    per_page_link = 5    # number of buttons in pagination
    context = eachPageObject(request, all_cost_list, obj_per_page, per_page_link)
    context['all_my_wallet'] = all_my_wallet
    context['all_cost_related']=all_cost_related_by_this_user
    context['total_cost']=total_cost_of_running_month['the_count']
    context['prev_month_first_date'] = str(prev_month_first_day)
    context['prev_month_last_date'] = str(prev_month_last_day)
    context['next_month_first_date'] = str(next_month_first_day)
    context['next_month_last_date'] = str(next_month_last_day)
    context['current_month'] = datetime.strftime(datetime.strptime(str(current_date), '%Y-%m-%d'), '%m-%Y')
    print(context)

    return render(request, template_name, context)


def editCost(request,id):
    template_name = 'costs/edit_cost.html'
    get_this_cost_data = Costs.objects.get(id=id)
    get_this_related_data = CostRelated.objects.get(id=get_this_cost_data.cost_related_id)

    if request.method == 'POST':

        get_this_related_data.short_info = request.POST.get('short_info')
        get_this_related_data.short_description = request.POST.get('short_description')
        get_this_related_data.cost_field = request.POST.get('cost_field')

        get_this_cost_data.amount = request.POST.get('amount')
        get_this_cost_data.description = request.POST.get('description')
        get_this_cost_data.cost_date = request.POST.get('cost_date')

        get_this_related_data.save()
        get_this_cost_data.save()

        return redirect('costs:my_costs')
    
    context ={
        "this_cost_related_data": get_this_related_data,
        "this_cost_data": get_this_cost_data
    }
    return render(request, template_name, context)

def deleteCost(request,id):
    
    get_this_cost_data = Costs.objects.get(id=id)
    get_this_related_data = CostRelated.objects.get(id=get_this_cost_data.cost_related_id)


    # get_this_related_data.delete()
    get_this_cost_data.delete()

    return redirect('costs:my_costs')

def SearchUserCostList(request):

    template_name = 'costs/cost_list.html'
    cost_related_form = CostRelatedForm
    cost_form = CostForm
    all_my_wallet = Wallet.objects.filter(wallet_of = request.user)
    if request.GET.get('date_to') == None and request.GET.get('date_from') == None:
        current_date = datetime.today()
    elif request.GET.get('date_from') == '' and request.GET.get('date_to') == '':
        current_date = datetime.today()
    elif request.GET.get('date_from') == '':
        current_date = datetime.strptime(request.GET.get('date_to'), '%Y-%m-%d')
    elif request.GET.get('date_to') == '':
        current_date = datetime.strptime(request.GET.get('date_from'), '%Y-%m-%d')
    else:
        current_date = datetime.strptime(request.GET.get('date_to'), '%Y-%m-%d')

    print(current_date)

    print(current_date)
    first_date_of_this_month, last_date_of_this_month, prev_month_first_day, prev_month_last_day, next_month_first_day, next_month_last_day = create_first_last_date(current_date)
    search_cost_title = request.GET.get('cost_type', '')
    search_cost_field = request.GET.get('cost_field', '')
    if request.GET.get('date_from') == '':
        search_date_from = datetime.strptime('2022-01-01', '%Y-%m-%d')
    else:
        search_date_from = datetime.strptime(request.GET.get('date_from'), '%Y-%m-%d')
    if request.GET.get('date_to') == '':
        search_date_to = date.today()
    else:
        search_date_to = datetime.strptime(request.GET.get('date_to'), '%Y-%m-%d')
    print(request.GET.get('wallet'))
    if request.GET.get('wallet') == '' or request.GET.get('wallet') == None:
        all_cost_list = Costs.objects.filter(cost_related_id__create_by_id = request.user.id, cost_related_id__short_info__icontains = search_cost_title, cost_related_id__cost_field__icontains = search_cost_field , cost_date__range = [search_date_from, search_date_to]).order_by('cost_date')
    else:
        all_cost_list = Costs.objects.filter(cost_related_id__create_by_id = request.user.id, cost_related_id__short_info__icontains = search_cost_title, cost_date__range = [search_date_from, search_date_to], wallet_id = request.GET.get('wallet')).order_by('cost_date')
    total_cost_of_running_month = all_cost_list.aggregate(the_count=Sum('amount'))
    print(total_cost_of_running_month['the_count'])
    all_cost_related_by_this_user = CostRelated.objects.filter(create_by_id= request.user.id)

    if request.method == "POST":
        cost_related_form = CostRelatedForm(request.POST)
        cost_form = CostForm(request.POST)
        wallet = Wallet.objects.get(id = int(request.POST.get('wallet')))
        cost_amount = int(request.POST.get('amount'))
        print("-------------")
        print(request.POST.get('select_short_info'))
        if cost_related_form.is_valid()==False and request.POST.get('select_short_info') != '':
            prev_cost_related_info = request.POST.get('select_short_info')
            print(cost_form)
            if cost_form.is_valid():
                cost_form = cost_form.save(commit=False)
                cost_form.cost_related_id_id = int(prev_cost_related_info)
                print(cost_form.cost_related_id_id)
                print(cost_form)
                cost_form.save()
                wallet.wallet_status = wallet.wallet_status - cost_amount
                wallet.save()
        else:
            if cost_related_form.is_valid():
                cost_related_form = cost_related_form.save(commit=False)
                cost_related_form.create_by_id = int(request.user.id)
                print(cost_related_form.create_by_id)
                cost_related_form.save()

                last_entry_by_this_user = CostRelated.objects.filter(create_by = request.user, short_info = cost_related_form.short_info).latest('created_at')
                print(cost_form)
                if cost_form.is_valid():
                    cost_form = cost_form.save(commit=False)
                    cost_form.cost_related_id_id = int(last_entry_by_this_user)
                    print(cost_form.cost_related_id_id)
                    print(cost_form)
                    cost_form.save()
                    wallet.wallet_status = wallet.wallet_status - cost_amount
                    wallet.save()

        return redirect('costs:my_costs')

    # print(context['current_month'])
    # print(all_my_wallet)
    obj_per_page = 100 # number of items to be shown per page
    per_page_link = 5    # number of buttons in pagination
    context = eachPageObject(request, all_cost_list, obj_per_page, per_page_link)
    context['all_my_wallet'] = all_my_wallet
    context['all_cost_related']=all_cost_related_by_this_user
    context['total_cost']=total_cost_of_running_month['the_count']
    context['prev_month_first_date'] = str(prev_month_first_day)
    context['prev_month_last_date'] = str(prev_month_last_day)
    context['next_month_first_date'] = str(next_month_first_day)
    context['next_month_last_date'] = str(next_month_last_day)
    context['current_month'] = datetime.strftime(datetime.strptime(str(current_date.date()), '%Y-%m-%d'), '%m-%Y')
    

    return render(request, template_name, context)
