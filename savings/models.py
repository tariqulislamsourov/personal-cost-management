from django.db import models
from account.models import User

# Create your models here.

class PredefinedWalletList(models.Model):
    name = models.CharField(max_length=20, null=False, blank=False)
    full_name = models.CharField(max_length=30, null=True, blank=True, default='')
    url = models.CharField(max_length=100, blank=True, null=True, default='')
    image = models.CharField(max_length=255, null=False, blank=False)
    created_at = models.DateField(auto_now_add=True)

class Wallet(models.Model):
    
    wallet_name = models.CharField(max_length=30, null=False, blank=False, default='')
    wallet_info = models.ForeignKey(PredefinedWalletList, blank=True, null=True, default=1, on_delete=models.CASCADE)
    wallet_number = models.CharField(max_length=24, null=True, blank=True)
    wallet_status = models.IntegerField(null=True, blank=True)
    wallet_of = models.ForeignKey(User, on_delete=models.CASCADE)
    created_at = models.DateField(auto_now_add=True)
    updated_at = models.DateField(auto_now=True)

    class Meta:
        unique_together = ("wallet_info", "wallet_of")

    def __int__(self):
        return self.id

class SavingRelated(models.Model):
    create_by = models.ForeignKey(User, on_delete=models.CASCADE)
    short_info = models.CharField(max_length=50, null=False, blank=False, default='')
    short_description = models.CharField(max_length=255, null=True, blank=True, default='')
    saving_where = models.CharField(max_length=50, null=True, blank=True, default='')
    created_at = models.DateField(auto_now_add=True)

    def __int__(self):
        return self.id

class Savings(models.Model):
    # saving_related_id = models.ForeignKey(SavingRelated, on_delete=models.CASCADE)
    amount = models.IntegerField(blank=False, null=False)
    description = models.CharField(max_length=255, null=False, blank=True, default="")
    wallet = models.ForeignKey(Wallet, on_delete=models.CASCADE)
    saving_date = models.DateField(null=True, blank=True)
    create_by = models.ForeignKey(User, on_delete=models.CASCADE)
    created_at = models.DateField(auto_now_add=True)
    updated_at = models.DateField(auto_now=True)

    def __int__(self):
        return self.id

