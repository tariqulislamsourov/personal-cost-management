from django.shortcuts import render, redirect
from django.db.models import Count, Sum
from income.forms import IncomeRelatedForm, IncomeForm
from income.models import Incomes, IncomeRelated
from costs.views import eachPageObject, create_first_last_date
from datetime import datetime, date, timedelta
from savings.models import Wallet
# Create your views here.
def UserIncomeList(request):
    template_name = 'income/income_list.html'
    income_related_form = IncomeRelatedForm
    income_form = IncomeForm
    if request.GET.get('date_to') == None and request.GET.get('date_from') == None:
        current_date = date.today()
    all_my_wallet = Wallet.objects.filter(wallet_of = request.user)
    first_date_of_this_month, last_date_of_this_month, prev_month_first_day, prev_month_last_day, next_month_first_day, next_month_last_day = create_first_last_date(current_date)
    all_income_list = Incomes.objects.filter(income_related_id__create_by_id = request.user.id, income_date__range = [first_date_of_this_month, last_date_of_this_month]).order_by('income_date')
    total_income_of_running_month = all_income_list.aggregate(the_count=Sum('amount'))
    all_income_related_by_this_user = IncomeRelated.objects.filter(create_by_id= request.user.id)

    if request.method == "POST":
        income_related_form = IncomeRelatedForm(request.POST)
        income_form = IncomeForm(request.POST)
        wallet = Wallet.objects.get(id = int(request.POST.get('wallet')))
        income_amount = int(request.POST.get('amount'))
        if income_related_form.is_valid()==False and request.POST.get('select_short_info') != '':
            prev_income_related_info = request.POST.get('select_short_info')
            print(income_form)
            if income_form.is_valid():
                income_form = income_form.save(commit=False)
                income_form.income_related_id_id = int(prev_income_related_info)
                print(income_form.income_related_id_id)
                print(income_form)
                income_form.save()
                wallet.wallet_status = wallet.wallet_status + income_amount
                wallet.save()
        else:
            if income_related_form.is_valid():
                income_related_form = income_related_form.save(commit=False)
                income_related_form.create_by_id = int(request.user.id)
                print(income_related_form.create_by_id)
                income_related_form.save()

                last_entry_by_this_user = IncomeRelated.objects.filter(create_by = request.user, short_info=income_related_form.short_info).latest('created_at')
                print(income_form)
                if income_form.is_valid():
                    income_form = income_form.save(commit=False)
                    income_form.income_related_id_id = int(last_entry_by_this_user)
                    print(income_form.income_related_id_id)
                    print(income_form)
                    income_form.save()
                    wallet.wallet_status = wallet.wallet_status + income_amount
                    wallet.save()
        return redirect('income:my_income')

    print(type(prev_month_first_day))

    obj_per_page = 31 # number of items to be shown per page
    per_page_link = 5    # number of buttons in pagination
    context = eachPageObject(request, all_income_list, obj_per_page, per_page_link)
    context['all_my_wallet'] = all_my_wallet
    context['all_income_related']=all_income_related_by_this_user
    context['total']=total_income_of_running_month['the_count']
    context['prev_month_first_date'] = str(prev_month_first_day)
    context['prev_month_last_date'] = str(prev_month_last_day)
    context['next_month_first_date'] = str(next_month_first_day)
    context['next_month_last_date'] = str(next_month_last_day)
    context['current_month'] = datetime.strftime(datetime.strptime(str(current_date), '%Y-%m-%d'), '%m-%Y')
    print(context)

    return render(request, template_name, context)

def editIncome(request,id):
    template_name = 'income/edit_income.html'
    get_this_income_data = Incomes.objects.get(id=id)
    get_this_related_data = IncomeRelated.objects.get(id=get_this_income_data.income_related_id)

    if request.method == 'POST':

        get_this_related_data.short_info = request.POST.get('short_info')
        get_this_related_data.short_description = request.POST.get('short_description')
        get_this_related_data.income_source = request.POST.get('income_source')

        get_this_income_data.amount = request.POST.get('amount')
        get_this_income_data.description = request.POST.get('description')
        get_this_income_data.income_date = request.POST.get('income_date')

        get_this_related_data.save()
        get_this_income_data.save()

        return redirect('income:my_income')
    
    context ={
        "this_income_related_data": get_this_related_data,
        "this_income_data": get_this_income_data
    }
    return render(request, template_name, context)

def deleteIncome(request,id):
    
    get_this_income_data = Incomes.objects.get(id=id)
    get_this_related_data = IncomeRelated.objects.get(id=get_this_income_data.income_related_id)


    # get_this_related_data.delete()
    get_this_income_data.delete()

    return redirect('income:my_income')


def SearchUserIncomeList(request):

    template_name = 'income/income_list.html'
    income_related_form = IncomeRelatedForm
    income_form = IncomeForm
    all_my_wallet = Wallet.objects.filter(wallet_of = request.user)
    if request.GET.get('date_to') == None and request.GET.get('date_from') == None:
        current_date = datetime.today()
    elif request.GET.get('date_from') == '' and request.GET.get('date_to') == '':
        current_date = datetime.today()
    elif request.GET.get('date_from') == '':
        current_date = datetime.strptime(request.GET.get('date_to'), '%Y-%m-%d')
    elif request.GET.get('date_to') == '':
        current_date = datetime.strptime(request.GET.get('date_from'), '%Y-%m-%d')
    else:
        current_date = datetime.strptime(request.GET.get('date_to'), '%Y-%m-%d')

    first_date_of_this_month, last_date_of_this_month, prev_month_first_day, prev_month_last_day, next_month_first_day, next_month_last_day = create_first_last_date(current_date)
    search_income_title = request.GET.get('income_title', '')
    search_income_field = request.GET.get('income_field', '')
    if request.GET.get('date_from') == '':
        search_date_from = datetime.strptime('2022-01-01', '%Y-%m-%d')
    else:
        search_date_from = datetime.strptime(request.GET.get('date_from'), '%Y-%m-%d')
    if request.GET.get('date_to') == '':
        search_date_to = date.today()
    else:
        search_date_to = datetime.strptime(request.GET.get('date_to'), '%Y-%m-%d')

    if request.GET.get('wallet') == '' or request.GET.get('wallet') == None:
        all_income_list = Incomes.objects.filter(income_related_id__create_by_id = request.user.id, income_related_id__short_info__icontains = search_income_title, income_related_id__income_source__icontains = search_income_field , income_date__range = [search_date_from, search_date_to]).order_by('income_date')
    else:
        all_income_list = Incomes.objects.filter(income_related_id__create_by_id = request.user.id, income_related_id__short_info__icontains = search_income_title, income_related_id__income_source__icontains = search_income_field , income_date__range = [search_date_from, search_date_to], wallet_id = request.GET.get('wallet')).order_by('income_date')
    total_income_of_running_month = all_income_list.aggregate(the_count=Sum('amount'))
    print(total_income_of_running_month['the_count'])
    all_income_related_by_this_user = IncomeRelated.objects.filter(create_by_id= request.user.id)

    if request.method == "POST":
        income_related_form = IncomeRelatedForm(request.POST)
        income_form = IncomeForm(request.POST)
        wallet = Wallet.objects.get(id = int(request.POST.get('wallet')))
        income_amount = int(request.POST.get('amount'))
        if income_related_form.is_valid()==False and request.POST.get('select_short_info') != '':
            prev_income_related_info = request.POST.get('select_short_info')
            print(income_form)
            if income_form.is_valid():
                income_form = income_form.save(commit=False)
                income_form.income_related_id_id = int(prev_income_related_info)
                print(income_form.income_related_id_id)
                print(income_form)
                income_form.save()
                wallet.wallet_status = wallet.wallet_status + income_amount
                wallet.save()
        else:
            if income_related_form.is_valid():
                income_related_form = income_related_form.save(commit=False)
                income_related_form.create_by_id = int(request.user.id)
                print(income_related_form.create_by_id)
                income_related_form.save()

                last_entry_by_this_user = IncomeRelated.objects.filter(create_by = request.user, short_info=income_related_form.short_info).latest('created_at')
                print(income_form)
                if income_form.is_valid():
                    income_form = income_form.save(commit=False)
                    income_form.income_related_id_id = int(last_entry_by_this_user)
                    print(income_form.income_related_id_id)
                    print(income_form)
                    income_form.save()
                    wallet.wallet_status = wallet.wallet_status + income_amount
                    wallet.save()

        return redirect('income:my_income')

    obj_per_page = 100 # number of items to be shown per page
    per_page_link = 5    # number of buttons in pagination
    context = eachPageObject(request, all_income_list, obj_per_page, per_page_link)
    context['all_my_wallet'] = all_my_wallet
    context['all_income_related']=all_income_related_by_this_user
    context['total']=total_income_of_running_month['the_count']
    context['prev_month_first_date'] = str(prev_month_first_day)
    context['prev_month_last_date'] = str(prev_month_last_day)
    context['next_month_first_date'] = str(next_month_first_day)
    context['next_month_last_date'] = str(next_month_last_day)
    context['current_month'] = datetime.strftime(datetime.strptime(str(current_date.date()), '%Y-%m-%d'), '%m-%Y')
    print(context)

    return render(request, template_name, context)
